$('document').ready(function(){
        localStorage.setItem("chat_status", "connecting")
        init();
})
var websocket;
function init(){
        websocket = new WebSocket("ws://localhost:8099/socket_server.php"); 
		websocket.onopen = function(event) {
            $('#status').text("Connection is established!");
            localStorage.setItem("chat_status", "connected")		
		}
		websocket.onmessage = function(event) {
			var Data = JSON.parse(event.data);
			console.log("onmessage")
			console.log(Data)
        toastr.success(Data.message)
        let chatLeft = '<li class="custom_li other">'+Data.message+'</li>';
        $('.messages').append(chatLeft);
			
		};
		
		websocket.onerror = function(event){
            $('#status').text("Problem due to some Error");
            reconnect()
		};
		websocket.onclose = function(event){
            console.log("event on close")
            console.log(event)
            $('#status').text("Connection Closed");
            reconnect();
		}; 
		
		$('#btn_send').on("click",function(event){
			event.preventDefault();
            if($('#username').val() == null || $('#username').val() == ""){
                alert("please input username")
                return;
            }
            if($('#text_message').val() == null || $('#text_message').val() == ""){
                alert("please input message")
                return;
            }		
			var messageJSON = {
				chat_user: $('#username').val(),
				chat_message: $('#text_message').val()
            };
            $('#username').hide();
            $('#text_message').val('');
			websocket.send(JSON.stringify(messageJSON));
		});
}

function reconnect(){
    axios.get('../backend/socket_server.php')
        .then(function (response) {
            console.log(response.data)
        })
        .catch(function (error) {
            console.log(error);
        });
    init();
}

function openForm() {
    document.getElementById("myForm").style.display = "block";
}
  
function closeForm() {
    document.getElementById("myForm").style.display = "none";
}


function openElement() {
    alert("test");
    var messages = element.find('.messages');
    var textInput = element.find('.text-box');
    element.find('>i').hide();
    element.addClass('expand');
    element.find('.chat').addClass('enter');
    var strLength = textInput.val().length * 2;
    textInput.keydown(onMetaAndEnter).prop("disabled", false).focus();
    element.off('click', openElement);
    element.find('.header button').click(closeElement);
    element.find('#sendMessage').click(sendNewMessage);
    messages.scrollTop(messages.prop("scrollHeight"));
}

function closeElement() {
    element.find('.chat').removeClass('enter').hide();
    element.find('>i').show();
    element.removeClass('expand');
    element.find('.header button').off('click', closeElement);
    element.find('#sendMessage').off('click', sendNewMessage);
    element.find('.text-box').off('keydown', onMetaAndEnter).prop("disabled", true).blur();
    setTimeout(function() {
        element.find('.chat').removeClass('enter').show()
        element.click(openElement);
    }, 500);
}

function createUUID() {
    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
}

function sendNewMessage() {
    var userInput = $('.text-box');
    var newMessage = userInput.html().replace(/\<div\>|\<br.*?\>/ig, '\n').replace(/\<\/div\>/g, '').trim().replace(/\n/g, '<br>');

    if (!newMessage) return;

    var messagesContainer = $('.messages');

    messagesContainer.append([
        '<li class="self">',
        newMessage,
        '</li>'
    ].join(''));

    // clean out old message
    userInput.html('');
    // focus on input
    userInput.focus();

    messagesContainer.finish().animate({
        scrollTop: messagesContainer.prop("scrollHeight")
    }, 250);
}

function onMetaAndEnter(event) {
    if ((event.metaKey || event.ctrlKey) && event.keyCode == 13) {
        sendNewMessage();
    }
}