-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 03, 2020 at 05:51 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_about_us`
--

CREATE TABLE `cms_about_us` (
  `id` int(11) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `phone_number` varchar(200) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cms_about_us`
--

INSERT INTO `cms_about_us` (`id`, `email`, `address`, `phone_number`, `title`, `description`, `image`) VALUES
(1, 'golden@gmail.com', 'bogor', '081517449175', 'Golden Boy', 'Salah satu metode pembayaran yang ditawarkan Midtrans adalah Transfer Bank. Dengan menggunakan metode pembayaran ini, customer akan mempunyai pilihan untuk melakukan pembayaran melalui transfer bank dan Midtrans akan mengirimkan notifikasi pemberitahuan secara real time setelah customer menyelesaikan pembayarannya.', '../image_upload/1594280062gopay.png'),
(2, 'goldend@gmail.com', 'Bogor', '081517449175', 'Golden Dead', 'Sejak tahun sekian-sekian', '../image_upload/8.png');

-- --------------------------------------------------------

--
-- Table structure for table `cms_galery`
--

CREATE TABLE `cms_galery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cms_galery`
--

INSERT INTO `cms_galery` (`id`, `title`, `description`, `image`) VALUES
(1, 'Aktivitas Jual beli Edit', 'Jual beli', '../image_upload/15940221982.png'),
(2, 'KajianOnline', 'Online', '../image_upload/15940222533.png'),
(3, 'Qurban-Qurban', 'QUrban Adalah salah satu ibadah dalam islam', '../image_upload/1594288203tuxedo.jpg'),
(4, 'Qurban', 'QUrban Adalah salah satu ibadah dalam islam', '../image_upload/1594288237look 1.png'),
(5, 'Qurban', 'QUrban Adalah salah satu ibadah dalam islam', '../image_upload/1594288914gopay.png'),
(6, 'Qurban', 'QUrban Adalah salah satu ibadah dalam islam', '../image_upload/1594288953tuxedo.jpg'),
(7, 'Galeri Qurban', 'Qurban', '../image_upload/1594880667detail.png'),
(8, 'Galeri Qurban', 'Qurban', '../image_upload/1594880682order.png'),
(9, 'Galeri Qurban', 'Qurban', '../image_upload/1594880698Screenshot from 2020-01-13 10-26-39.png'),
(10, 'Galeri Qurban', 'Qurban', '../image_upload/1594880723product-CGNy4JkYwL54FiX@2x.png');

-- --------------------------------------------------------

--
-- Table structure for table `cms_product`
--

CREATE TABLE `cms_product` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cms_product`
--

INSERT INTO `cms_product` (`id`, `title`, `description`, `image`) VALUES
(1, 'Product SHoes', 'Shoes', '../image_upload/15940226741.png'),
(2, 'Suit', 'suit suit swiw', '../image_upload/1594712354look 1.png'),
(3, 'Vest', 'vest vest vespa', '../image_upload/1594712392payment-method.png'),
(4, 'SHoes 2', 'Shoes 2 adalah', '../image_upload/1594712652tuxedo.jpg'),
(5, 'suit 2', 'suit 2 adalah', '../image_upload/1594712680WhatsApp Image 2019-12-12 at 10.41.52 (1).jpeg'),
(6, 'unit 2', 'unit 2 adalah', '../image_upload/1594712701WhatsApp Image 2019-12-12 at 10.41.52 (1).jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `cms_question`
--

CREATE TABLE `cms_question` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_number` varchar(25) DEFAULT NULL,
  `question` text DEFAULT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cms_question`
--

INSERT INTO `cms_question` (`id`, `email`, `phone_number`, `question`, `answer`) VALUES
(1, 'awal@gmail.com', '081517532322', 'Kajian yuk yuk?', '            <strong>W3Schools</strong> is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using <b>this site</b>, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2020 by Refsnes Data. All Rights Reserved.\nPowered by W3.CSS.\n\nW3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2020 by Refsnes Data. All Rights Reserved. Powered by W3.CSS.\n\nW3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2020 by Refsnes Data. All Rights Reserved. Powered by W3.CSS.\n\nW3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2020 by Refsnes Data. All Rights Reserved. Powered by W3.CSS.\n\nW3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2020 by Refsnes Data. All Rights Reserved. Powered by W3.CSS.\n\nW3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2020 by Refsnes Data. All Rights Reserved. Powered by W3.CSS.\n\nW3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2020 by Refsnes Data. All Rights Reserved. Powered by W3.CSS.\n\nW3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy. Copyright 1999-2020 by Refsnes Data. All Rights Reserved. Powered by W3.CSS.'),
(2, 'muhib@gmail.com', '081517449175', 'Kenapa harus mengajarkan tauhid?', ''),
(3, 'halim@gmail.com', '081517449175', 'seberapa pentingkah mengajarkan tauhid?', 'Karena tauhid adalah kunci masuk surga');

-- --------------------------------------------------------

--
-- Table structure for table `cms_user`
--

CREATE TABLE `cms_user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cms_user`
--

INSERT INTO `cms_user` (`id`, `username`, `password`) VALUES
(3, 'mujib@gmail.com', 'asadfs'),
(4, 'testawa@gmail.com', 'asadfs'),
(5, 'testawa@gmail.com', 'asadfs'),
(6, 'yayang@gmail.com', 'asadfs'),
(7, 'cantik@gmail.com', 'asadfs'),
(8, 'testawa@gmail.com', 'asadfs'),
(9, 'testawa@gmail.com', 'asadfs'),
(10, 'testawa@gmail.com', 'asadfs'),
(11, 'testawa@gmail.com', 'asadfs'),
(13, 'testawa@gmail.com', 'asadfs'),
(14, 'testawa@gmail.com', 'asadfs'),
(15, 'testawa@gmail.com', 'asadfs');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_about_us`
--
ALTER TABLE `cms_about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_galery`
--
ALTER TABLE `cms_galery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_product`
--
ALTER TABLE `cms_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_question`
--
ALTER TABLE `cms_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_user`
--
ALTER TABLE `cms_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_about_us`
--
ALTER TABLE `cms_about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_galery`
--
ALTER TABLE `cms_galery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cms_product`
--
ALTER TABLE `cms_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cms_question`
--
ALTER TABLE `cms_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cms_user`
--
ALTER TABLE `cms_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
