<?php
include '../config/connection.php';
session_start();
$method = $_SERVER['REQUEST_METHOD'];
// $request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
//$input = json_decode(file_get_contents('php://input'),true);


if (!$con) {
  die("Connection failed: " . mysqli_connect_error());
}


switch ($method) {
    case 'GET':
      if(!isset($_GET['id'])){
        $sql = "select * from cms_galery order by id asc"; 
      }else{
        $id = $_GET['id'];
        $sql = "select * from cms_galery".($id?" where id=$id":''); 
      }
      
      break;
    case 'POST':
      $json = file_get_contents('php://input');
      $input = json_decode($json);
      $title = $input->title;
      $description = $input->description;
      $image = $input->image;

      $sql = "insert into cms_galery (title, description, image) values ('$title', '$description','$image')"; 
      break;
    case 'PUT':
      $id = $_GET['id'];
      $json = file_get_contents('php://input');
      $input = json_decode($json);
      $title = $input->title;
      $description = $input->description;
      $image = $input->image;

      $sql = "update cms_galery set title='$title', description='$description', image='$image'  where id = $id"; 
      break;
    case 'DELETE':
      $id = $_GET['id'];
      $sql = "delete from cms_galery where id=$id";
    break;

}

// run SQL statement
$result = mysqli_query($con,$sql);

// die if SQL statement failed
if (!$result) {
  http_response_code(404);
  die(mysqli_error($con));
}

if ($method == 'GET') {
    if (!$id) echo '[';
    for ($i=0 ; $i<mysqli_num_rows($result) ; $i++) {
      echo ($i>0?',':'').json_encode(mysqli_fetch_object($result));
    }
    if (!$id) echo ']';
  } elseif ($method == 'POST') {
    echo json_encode($result);
  } else {
    echo mysqli_affected_rows($con);
  }

$con->close();