<?php
include '../config/connection.php';

$method = $_SERVER['REQUEST_METHOD'];
// $request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
//$input = json_decode(file_get_contents('php://input'),true);


if (!$con) {
  die("Connection failed: " . mysqli_connect_error());
}

if (!$con) {
  die("Connection failed: " . mysqli_connect_error());
}
$response;
$timemilis = time();
$target_dir = "../image_upload/";
$target_file = $target_dir . $timemilis. basename($_FILES["fileUpload"]["name"]);
// $target_file = $target_dir . basename($_FILES["fileUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image
// if(isset($_POST["submit"])) {
  $check = getimagesize($_FILES["fileUpload"]["tmp_name"]);
  if($check !== false) {
    // $response->result = null;
    // $response->status = "failed";
    // $response->message = "File is an image - " . $check["mime"] . ".";
    // echo json_encode($response);
    // return;
    $uploadOk = 1;
  } else {
    $response['result'] = null;
    $response['status'] = "failed";
    $response['message'] = "File is not an image.";
    echo json_encode($response);
    return;
    $uploadOk = 0;
  }
// }

// Check if file already exists
if (file_exists($target_file)) {
  $response['result'] = null;
  $response['status'] = "failed";
  $response['message'] = "Sorry, file already exists.";
    echo json_encode($response);
  $uploadOk = 0;
}

// Check file size
if ($_FILES["fileUpload"]["size"] > 500000) {
  $response['result'] = null;
  $response['status'] = "failed";
  $response['message'] = "Sorry, your file is too large.";
    echo json_encode($response);
    return;
  $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
  $response['result'] = null;
    $response['status'] = "failed";
    $response['message'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    echo json_encode($response);
    return;
  $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
  $response['result'] = null;
  $response['status'] = "failed";
  $response['message'] = "Sorry, your file was not uploaded.";
    echo json_encode($response);
    return;
// if everything is ok, try to upload file
} else {
  if (move_uploaded_file($_FILES["fileUpload"]["tmp_name"], $target_file)) {
    $response['result'] = $target_file;
    $response['status'] = "success";
    $response['message'] = "The file ". basename( $_FILES["fileUpload"]["name"]). " has been uploaded.".$target_file;
    echo json_encode($response);
    return;
  } else {
    $response['result'] = null;
    $response['status'] = "failed";
    $response['message'] = "Sorry, there was an error uploading your file.";
    echo json_encode($response);
    return;
  }
}

$con->close();