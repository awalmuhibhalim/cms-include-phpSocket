<?php
include '../config/connection.php';

$method = $_SERVER['REQUEST_METHOD'];
// $request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
//$input = json_decode(file_get_contents('php://input'),true);


if (!$con) {
  die("Connection failed: " . mysqli_connect_error());
}


switch ($method) {
    case 'GET':
      if(!isset($_GET['id'])){
        $sql = "select * from cms_question order by id asc"; 
      }else{
        $id = $_GET['id'];
        $sql = "select * from cms_question".($id?" where id=$id":''); 
      }
      
      break;
    case 'POST':
      $json = file_get_contents('php://input');
      $input = json_decode($json);
      $email = $input->email;
      $phone_number = $input->phone_number;
      $question = $input->question;
      $answer = $input->answer;

// $json = json_decode($json);
// $lat = $json->results[0]->geometry->location->lat;
// $lng = $json->results[0]->geometry->location->lng;
// echo "Latitude: " . $lat . ", Longitude: " . $lng;

      // $name = $_POST["name"];
      // $email = $_POST["email"];
      // $country = $_POST["country"];
      // $city = $_POST["city"];
      // $job = $_POST["job"];

      $sql = "insert into cms_question (email, phone_number, question, answer) values ('$email', '$phone_number', '$question', '$answer')"; 
      break;
    case 'PUT':
      $id = $_GET['id'];
      $json = file_get_contents('php://input');
      $input = json_decode($json);
      $email = $input->email;
      $phone_number = $input->phone_number;
      $question = $input->question;
      $answer = $input->answer;

      $sql = "update cms_question set email='$email', phone_number='$phone_number', question='$question', answer='$answer' where id = $id"; 
      break;
    case 'DELETE':
      $id = $_GET['id'];
      $sql = "delete from cms_question where id=$id";
    break;

}

// run SQL statement
$result = mysqli_query($con,$sql);

// die if SQL statement failed
if (!$result) {
  http_response_code(404);
  die(mysqli_error($con));
}

if ($method == 'GET') {
    if (!$id) echo '[';
    for ($i=0 ; $i<mysqli_num_rows($result) ; $i++) {
      echo ($i>0?',':'').json_encode(mysqli_fetch_object($result));
    }
    if (!$id) echo ']';
  } elseif ($method == 'POST') {
    echo json_encode($result);
  } else {
    echo mysqli_affected_rows($con);
  }

$con->close();