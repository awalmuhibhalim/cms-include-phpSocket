<?php
  include 'header.php'
?>
<div id='app' class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
              <h3 class="m-0 text-dark">Product</h3><br>
                <button class="btn btn-primary" @click="fOpenForm()">Add New</button>
              </div>
              <!-- Start Card Body -->
              <div class="card-body">
                <form role="form">
                      <table id="example2" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Image</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr v-for="elm in list" :key="elm">
                            <td>{{elm.title}}</td>
                            <td>{{elm.description}}</td>
                            <td>{{elm.image}}</td>
                            <td>
                              <div class="btn btn-primary" @click.privent="fGetDetail(elm.id)">
                                <i class="fa fa-book" aria-hidden="true"></i>
                              </div>&nbsp;
                              <div class="btn btn-danger" @click.privent="fDelete(elm.id)">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tr>
                        </tfoot>
                      </table>
                </form>
              </div>
              <!-- End Card Body -->
            </div>
          </div>
            <!-- Start Modal -->
            <div class="modal fade" id="myModal">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Form</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form role="form">
                        <div class="card-body">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input type="email" class="form-control" v-model="form.title" placeholder="Enter email">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Description</label>
                            <textarea class="form-control" v-model="form.description"></textarea>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputFile">File input</label><br>
                            <img v-if="form.image != null" v-bind="{src:form.image}" class="img-thumbnail"  alt="">
                            <div class="input-group">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image" @change="filesUpload">
                                <label class="custom-file-label" for="image">Choose file</label>
                              </div>
                              <div class="input-group-append">
                                <span class="input-group-text" id="">Upload</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>    
                    </div>
                    <div class="modal-footer justify-content-between">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary" @click.prevent="fSubmit()">Save</button>
                    </div>
                  </div>
                </div>
            </div>
          <!-- End Modal -->
        </div>
      </div>
    </section>
  </div>
 <script>
var app = new Vue({
  el: '#app',
  data: {
      form : {
        id : null,
        title: null,
        description : null,
        image : null,
      },
      list : [],
      file : null
  },
  mounted: function () {
    console.log('Hello from Vue!')
    this.fGetAll()
    
  },

  methods: {
    fGetAll: function(){
        axios.get('../backend/be_product.php')
        .then(function (response) {
            app.list = response.data;
        })
        .catch(function (error) {
            console.log(error);
        });
    },
    fOpenForm(){
      this.resetForm();
      $('#myModal').modal('show');
      
    },
    fGetDetail : function(id){
      axios.get('../backend/be_product.php?id='+id)
        .then(function (response) {
            app.form = response.data;
            $('#myModal').modal('show');

        })
        .catch(function (error) {
            console.log(error);
        });
    },
    fSubmit: function(){
        this.uploading();        
    },
    fCreate(){
      if(this.form.id){
            axios({
              method: 'PUT',
              url: '../backend/be_product.php?id='+this.form.id,
              data: this.form,
              config: { 
                headers: {'Content-Type': 'application/json' }
              }
          })
          .then(function (response) {
              // app.resetForm();
              alert("sukses")
          })
          .catch(function (response) {
              // alert(response.data.message;)
          });
        }else{
            axios({
              method: 'POST',
              url: '../backend/be_product.php',
              data: this.form,
              config: { 
                headers: {'Content-Type': 'application/json' }
              }
          })
          .then(function (response) {
            console.log("response")
            console.log(response)
            alert("sukses")
          })
          .catch(function (response) {
              // alert(response.data.message;)
          });
        }
    },
    fDelete(id){
      axios.delete('../backend/be_product.php?id='+id)
        .then(function (response) {
            alert('success')
        })
        .catch(function (response) {
          // alert(response.data.message;)
          });
    },
    resetForm: function(){
          this.form.id = null;
          this.form.title = null;
          this.form.description = null;
          this.form.image = null;
    },
    filesUpload(e){
      var files = e.target.files || e.dataTransfer.files;
      let formData = new FormData();
          formData.append('fileUpload', files[0]);
      this.file = formData;
    },
    uploading(){
      let flag = false;
      let self = this;
      if(this.file != null){
          axios.post('../backend/upload.php',this.file,{
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(function(response){
                self.form.image = response.data.result; 
                flag = true;
            }).catch(function (response) {
              console.log("response upload");
              console.log(response)
              // alert(response.data.message;)
          });
        }
      
      let interval = setInterval(function(){
        if(this.file == null){
          self.fCreate();
          clearInterval(interval);
        }else if(this.file != null){
            if(flag){
              self.fCreate();
              clearInterval(interval);
            }
        }
      }, 3000);
      
    }

  }
})    
</script>
<?php
  include 'footer.php'
?>