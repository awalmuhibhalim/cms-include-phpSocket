<?php
  include 'header.php'
?>
<div id='app' class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
              <h3 class="m-0 text-dark">User</h3><br>
                <button class="btn btn-primary" @click="fOpenForm()">Add New</button>
              </div>
              <!-- Start Card Body -->
              <div class="card-body">
                <form role="form">
                      <table id="example2" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr v-for="elm in list" :key="elm">
                            <td>{{elm.username}}</td>
                            <td>{{elm.password}}</td>
                            <td>
                              <div class="btn btn-primary" @click.privent="fGetDetail(elm.id)">
                                <i class="fa fa-book" aria-hidden="true"></i>
                              </div>&nbsp;
                              <div class="btn btn-danger" @click.privent="fDelete(elm.id)">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>Rendering engine</th>
                            <th>Browser</th>
                            <!-- <th>Browser</th> -->
                          </tr>
                        </tfoot>
                      </table>
                </form>
              </div>
              <!-- End Card Body -->
            </div>
          </div>
            <!-- Start Modal -->
            <div class="modal fade" id="myModal">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Form</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form role="form">
                        <div class="card-body">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" v-model="form.username" placeholder="Enter email">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" v-model="form.password" placeholder="Password">
                          </div>
                        </div>
                      </form>    
                    </div>
                    <div class="modal-footer justify-content-between">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary" @click.prevent="fCreate()">Save</button>
                    </div>
                  </div>
                </div>
            </div>
          <!-- End Modal -->
        </div>
      </div>
    </section>
  </div>
 <script>
var app = new Vue({
  el: '#app',
  data: {
      form : {
        id : null,
        username: '',
        password: '',
      },
      list : []
  },
  mounted: function () {
    console.log('Hello from Vue!')
    this.fGetAll()
    
  },

  methods: {
    fGetAll: function(){
        axios.get('../backend/be_user.php')
        .then(function (response) {
            app.list = response.data;
        })
        .catch(function (error) {
            console.log(error);
        });
    },
    fOpenForm(){
      this.resetForm();
      $('#myModal').modal('show');
      
    },
    fGetDetail : function(id){
      axios.get('../backend/be_user.php?id='+id)
        .then(function (response) {
            app.form = response.data;
            $('#myModal').modal('show');

        })
        .catch(function (error) {
            console.log(error);
        });
    },
    fSubmit: function(){
        this.uploading();        
    },
    fCreate(){
      if(this.form.id){
            axios({
              method: 'PUT',
              url: '../backend/be_user.php?id='+this.form.id,
              data: this.form,
              config: { 
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization': 'Bearer Bos'
                  }
              }
          })
          .then(function (response) {
            app.notifSuccess("Success")
          })
          .catch(function (response) {
              app.notifError(response.data)
          });
        }else{
            axios({
              method: 'POST',
              url: '../backend/be_user.php',
              data: this.form,
              config: { 
                headers: {'Content-Type': 'application/json' }
              }
          })
          .then(function (response) {
            app.notifSuccess("Success")
          })
          .catch(function (response) {
            app.notifError(response.data)
          });
        }
    },
    fDelete(id){
      axios.delete('../backend/be_user.php?id='+id)
        .then(function (response) {
          app.notifSuccess("Success")
        })
        .catch(function (error) {
            app.notifError(error.data)
        });
    },
    resetForm: function(){
        this.form.id = null;
        this.form.username = null;
        this.form.password = null;
    },
    filesUpload(e){
      var files = e.target.files || e.dataTransfer.files;
      let formData = new FormData();
          formData.append('fileUpload', files[0]);
      this.file = formData;
    },
    uploading(){
      let flag = false;
      let self = this;
      if(this.file != null){
          axios.post('api/upload.php',this.file,{
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(function(response){
                self.form.image = response.data.result; 
                flag = true;
            }).catch(function(){
              app.notifError(response.data)
          });
        }
      
      let interval = setInterval(function(){
        if(this.file == null){
          self.createContact();
          clearInterval(interval);
        }else if(this.file != null){
            if(flag){
              self.createContact();
              clearInterval(interval);
            }
        }
      }, 3000);
    },
    notifError(message){
      toastr.error(message)
    },
    notifSuccess(message){
      toastr.success(message)
    }
  }
})    
</script>
<?php
  include 'footer.php'
?>