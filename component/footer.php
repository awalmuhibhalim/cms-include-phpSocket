<aside class="control-sidebar control-sidebar-dark"></aside>
  <!-- <div class="row">
    <div class="col-md-7"></div>
    <div class="col-md-5" style="margin-bottom:100px">
          <div id="chat_container">
            <p id="status"></p>
            <div class="custom_container">
              <img src="/w3images/bandmember.jpg" alt="Avatar" style="width:100%;">
              <p>Hello. How are you today?</p>
              <span class="time-right">11:00</span>
            </div>

            <div class="custom_container darker">
              <img src="/w3images/avatar_g2.jpg" alt="Avatar" class="right" style="width:100%;">
              <p>Hey! I'm fine. Thanks for asking!</p>
              <span class="time-left">11:01</span>
            </div>
          </div> 
          <div class="row">
            <div class="col-md-10">
              <div class="form-group">
                <input type="text" id="text_message" class="form-control" placeholder="type a message..."/>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <button class="btn btn-primary" id="btn_send">Send</button>
              </div>
            </div>
          </div>
    </div>
  </div> -->

  


  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.5
    </div>

    <button class="open-button" onclick="openForm()"><img src="../image_upload/chat.png" class="img-fluid" style="width: 50px;"></button>
    <div class="chat-popup" id="myForm"><i style="float: right; background-color: red;width: 20px;text-align: center;" onclick="closeForm()">x</i>
      <form class="form-container">
        <img src="../image_upload/Chatting-PNG-Image.png" class="img-fluid" style="width: 40px;">&nbsp;&nbsp;<i>Live Chat</i><br><br>
        <div class="chat_container">
            <p id="status"></p>
            <ul class="messages">
              <li class="custom_li other">asdasdasasdasdasasdasdasasdasdasasdasdasasdasdasasdasdas</li>
              <li class="custom_li other">Are we dogs??? 🐶</li>
              <li class="custom_li self">no... we're human</li>
              <li class="custom_li other">are you sure???</li>
              <li class="custom_li self">yes.... -___-</li>
            </ul>
        </div>
        <input type="text" id="username" placeholder="input your username" class="form-control"/>
        <textarea placeholder="Type message.." name="msg" id="text_message" required></textarea>

        <button id="btn_send" class="btn">Send</button>
      </form>
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->

<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="../dist/js/demo.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="../plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="../plugins/raphael/raphael.min.js"></script>
<script src="../plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="../plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="../plugins/chart.js/Chart.min.js"></script>

<!-- PAGE SCRIPTS -->
<!-- <script src="../dist/js/pages/dashboard2.js"></script> -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<!-- SweetAlert2 -->
<script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="../plugins/toastr/toastr.min.js"></script>
<script src="../config/chat.js"></script>
<script>
  $(function () {
    $('#example2').DataTable({
      "responsive": true,
      "autoWidth": false
    });
  });


  // $(document).ready(function(){
	// 	var websocket = new WebSocket("ws://localhost:8099/socket_custom.php"); 
	// 	websocket.onopen = function(event) {
  //     $('#status').text("Connection is established!");		
	// 	}
	// 	websocket.onmessage = function(event) {
	// 		var Data = JSON.parse(event.data);
	// 		console.log("onmessage")
	// 		console.log(Data)
  //     toastr.success(Data.message)
  //     let chatLeft = '<li class="custom_li other">'+Data.message+'</li>';
  //     $('.messages').append(chatLeft);

	// 		$('#text_message').val('');
	// 	};
		
	// 	websocket.onerror = function(event){
  //     $('#status').text("Problem due to some Error");
	// 	};
	// 	websocket.onclose = function(event){
  //     $('#status').text("Connection Closed");
	// 	}; 
		
	// 	$('#btn_send').on("click",function(event){
	// 		event.preventDefault();
	// 		// $('#chat-user').attr("type","hidden");		
	// 		var messageJSON = {
	// 			chat_user: "Awal",
	// 			chat_message: $('#text_message').val()
	// 		};
	// 		websocket.send(JSON.stringify(messageJSON));
	// 	});
	// });
</script>
<style>
  .custom_container {
  border: 2px solid #dedede;
  background-color: #f1f1f1;
  border-radius: 5px;
  padding: 10px;
  margin: 10px 0;
}

.darker {
  border-color: #ccc;
  background-color: #ddd;
}

.custom_container::after {
  content: "";
  clear: both;
  display: table;
}

.custom_container img {
  float: left;
  max-width: 60px;
  width: 100%;
  margin-right: 20px;
  border-radius: 50%;
}

.custom_container img.right {
  float: right;
  margin-left: 20px;
  margin-right:0;
}

.time-right {
  float: right;
  color: #aaa;
}

.time-left {
  float: left;
  color: #999;
}
</style>
</body>
</html>
