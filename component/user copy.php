<?php
  include 'header.php'
?>
<div id='app' class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0 text-dark">User</h1>
            <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add New</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title">User</h5>
                
              </div>
              <div class="card-body">
                <!-- <form role="form"> -->
                      <table id="example2" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr v-for="elm in list" :key="elm">
                            <td>{{elm.username}}</td>
                            <td>{{elm.password}}</td>
                            <td><button class="btn btn-primary" @click="getUser(elm.id)" >edit</button>&nbsp;<button class="btn btn-danger">delete</button></td>
                          </tr>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>Rendering engine</th>
                            <th>Browser</th>
                          </tr>
                        </tfoot>
                      </table>
                <!-- </form> -->
                
                <div class="modal fade" id="myModal">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Default Modal</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <form role="form">
                        <div class="card-body">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" v-model="form.username" placeholder="Enter email">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" v-model="form.password" placeholder="Password">
                          </div>
                        </div>
        
                        <!-- <div class="card-footer">
                          <button class="btn btn-primary" @click.prevent="createUser()">Submit</button>
                        </div> -->
                      </form>    
                      </div>
                      <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click.prevent="createUser()">Save</button>
                      </div>
                    </div>
                  </div>
              </div>
              </div>
              <div class="card-footer">
                
              </div>
            </div>
          </div>
          </div>
        </div>
    </section>
  </div>
 <script>
var app = new Vue({
  el: '#app',
  data: {
      form : {
        id : null,
        username: '',
        password: '',
      },
      list : []
  },
  mounted: function () {
    console.log('Hello from Vue!')
    this.getAlluser()
    
  },

  methods: {
    getAlluser: function(){
        axios.get('../backend/be_user.php')
        .then(function (response) {
            app.list = response.data;

        })
        .catch(function (error) {
            console.log(error);
        });
    },
    getUser : function(id){
      axios.get('../backend/be_user.php?id='+id)
        .then(function (response) {
            app.form = response.data;
            $('#myModal').modal('show');

        })
        .catch(function (error) {
            console.log(error);
        });
    },
    submit: function(){
        this.uploading();        
    },
    createUser(){
      
      if(this.form.id){
            axios({
              method: 'PUT',
              url: '../backend/be_user.php?id='+this.form.id,
              data: this.form,
              config: { 
                headers: {'Content-Type': 'application/json' }
              }
          })
          .then(function (response) {
              // app.resetForm();
              alert("sukses")
          })
          .catch(function (response) {
              console.log(response)
          });
        }else{
            axios({
              method: 'POST',
              url: '../backend/be_user.php',
              data: this.form,
              config: { 
                headers: {'Content-Type': 'application/json' }
              }
          })
          .then(function (response) {
            console.log("response")
            console.log(response)
            alert("sukses")
          })
          .catch(function (response) {
              console.log(response)
          });
        }
    },
    deleteContact(){
      alert("here "+this.form.id);
      axios.delete('../backend/be_user.php?id='+this.form.id)
        .then(function (response) {
            app.contacts = response.data;

        })
        .catch(function (error) {
            console.log(error);
        });
    },
    resetForm: function(){
        this.form.name = null;
        this.form.email = null;
        this.form.country = null;
        this.form.city = null;
        this.form.job = null;
        this.form.image = null;
    },
    getDetail(id){
      axios.get('api/crud.php?id='+id)
        .then(function (response) {
            console.log(response.data);
            app.form = response.data;

        })
        .catch(function (error) {
            console.log(error);
        });
    },
    filesUpload(e){
      var files = e.target.files || e.dataTransfer.files;
      let formData = new FormData();
          formData.append('fileUpload', files[0]);
      this.file = formData;
    },
    uploading(){
      let flag = false;
      let self = this;
      if(this.file != null){
          axios.post('api/upload.php',this.file,{
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(function(response){
                self.form.image = response.data.result; 
                flag = true;
            }).catch(function(){
                console.log('FAILURE!!');
          });
        }
      
      let interval = setInterval(function(){
        if(this.file == null){
          self.createContact();
          clearInterval(interval);
        }else if(this.file != null){
            if(flag){
              self.createContact();
              clearInterval(interval);
            }
        }
      }, 3000);
      
    }

  }
})    
</script>
<?php
  include 'footer.php'
?>