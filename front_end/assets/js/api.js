var app = new Vue({
    el: '#app',
    data: {
        form : {
          id : null,
          title: null,
          email: null,
          address : null,
          phone_number : null,
          description : null,
          image : null,
        },
        list : [],
        about_us : [],
        product : [],
        galery : [],
        member : "there is no member",
        questions : [],
        answer : "",

        file : null
    },
    mounted: function () {
      window.self = this.runningSocket();
      console.log('Hello from Vue!')
      this.runningSocket();
      this.fGetAll()
      this.fGetAboutUs();
      this.fGetProduct();
      this.fGetGalery();
      this.fGetQuestion();
      
    },
  
    methods: {
      runningSocket: function(){
        axios.get('../backend/socket_server.php')
        // .then(function (response) {
        // console.log(response.data)
        // })
        // .catch(function (error) {
        //     console.log(error);
        // });
      },
      fGetAll: function(){
          axios.get('../backend/be_about_us.php')
          .then(function (response) {
              app.list = response.data;
              console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          });
      },
      fGetAboutUs: function(){
        axios.get('../backend/be_about_us.php')
        .then(function (response) {
            app.about_us = response.data;
            console.log(response.data)
        })
        .catch(function (error) {
            console.log(error);
        });
      },
      fGetProduct: function(){
        axios.get('../backend/be_product.php')
        .then(function (response) {
            app.product = response.data;
            console.log(response.data)
        })
        .catch(function (error) {
            console.log(error);
        });
      },
      fGetGalery: function(){
        axios.get('../backend/be_galery.php')
        .then(function (response) {
            app.galery = response.data;
            console.log(response.data)
        })
        .catch(function (error) {
            console.log(error);
        });
      },
      fGetQuestion: function(){
        axios.get('../backend/be_question.php')
        .then(function (response) {
            app.questions = response.data;
            console.log(response.data)
        })
        .catch(function (error) {
            console.log(error);
        });
      },
      fOpenForm(text){
        // this.resetForm();
        // alert("test")
        $('#myModal').modal('show');
        this.answer = text;
        
      },
      fGetDetail : function(id){
        axios.get('../backend/be_about_us.php?id='+id)
          .then(function (response) {
              app.form = response.data;
              $('#myModal').modal('show');
  
          })
          .catch(function (error) {
              console.log(error);
          });
      },
      fSubmit: function(){
          this.uploading();        
      },
      fCreate(){
        if(this.form.id){
              axios({
                method: 'PUT',
                url: '../backend/be_about_us.php?id='+this.form.id,
                data: this.form,
                config: { 
                  headers: {'Content-Type': 'application/json' }
                }
            })
            .then(function (response) {
                // app.resetForm();
                alert("sukses")
            })
            .catch(function (response) {
                console.log(response)
            });
          }else{
              axios({
                method: 'POST',
                url: '../backend/be_about_us.php',
                data: this.form,
                config: { 
                  headers: {'Content-Type': 'application/json' }
                }
            })
            .then(function (response) {
              console.log("response")
              console.log(response)
              alert("sukses")
            })
            .catch(function (response) {
                console.log(response)
            });
          }
      },
      fDelete(id){
        axios.delete('../backend/be_about_us.php?id='+id)
          .then(function (response) {
              alert('success')
          })
          .catch(function (error) {
              console.log(error);
          });
      },
      resetForm: function(){
            this.form.id = null;
            this.form.title = null;
            this.form.email = null;
            this.form.address = null;
            this.form.phone_number = null;
            this.form.description = null;
            this.form.image = null;
      },
      filesUpload(e){
        var files = e.target.files || e.dataTransfer.files;
        let formData = new FormData();
            formData.append('fileUpload', files[0]);
        this.file = formData;
      },
      uploading(){
        let flag = false;
        let self = this;
        if(this.file != null){
            axios.post('../backend/upload.php',this.file,{
                  headers: {
                      'Content-Type': 'multipart/form-data'
                  }
              }).then(function(response){
                  self.form.image = response.data.result; 
                  flag = true;
              }).catch(function(){
                  console.log('FAILURE!!');
            });
          }
        
        let interval = setInterval(function(){
          if(this.file == null){
            self.fCreate();
            clearInterval(interval);
          }else if(this.file != null){
              if(flag){
                self.fCreate();
                clearInterval(interval);
              }
          }
        }, 3000);
        
      }
  
    }
  })   